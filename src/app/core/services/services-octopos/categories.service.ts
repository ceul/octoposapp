import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import BaseService from './base-service';
import { Observable } from 'rxjs';

@Injectable()
export class CategoriesService extends BaseService {

	constructor(public http: HttpClient) {
		super(http);
	}
	
	getRootCategories() {
		try{
			return this.consumeService('/categories/get-root');
		}
		catch  (error) {
			console.log(error);
		}
	}

	getSubCategories(category: string) {
		try{
			return this.consumeService('/categories/get-sub', {category});
		}
		catch  (error) {
			console.log(error);
		}
	}

}