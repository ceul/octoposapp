import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import BaseService from './base-service';
import { Observable } from 'rxjs';

@Injectable()
export class UserService extends BaseService {

	constructor(public http: HttpClient) {
		super(http);
	}
	
	obtenerUsuarios() {
		try{
			return this.consumeService('/user/get');
		}
		catch  (error) {
			console.log(error);
		}
	}

}