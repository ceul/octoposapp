import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import BaseService from './base-service';
import { Observable } from 'rxjs';

@Injectable()
export class ProductsService extends BaseService {

	constructor(public http: HttpClient) {
		super(http);
	}
	
	getProductCatalog(category: string) {
		try{
			return this.consumeService('/product/get-catalog',{category});
		}
		catch  (error) {
			console.log(error);
		}
	}

}