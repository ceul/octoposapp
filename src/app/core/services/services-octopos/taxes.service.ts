import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import BaseService from './base-service';
import { Observable } from 'rxjs';

@Injectable()
export class TaxesService extends BaseService {

	constructor(public http: HttpClient) {
		super(http);
	}
	
	getTaxes() {
		try{
			return this.consumeService('/tax/get');
		}
		catch  (error) {
			console.log(error);
		}
	}

}