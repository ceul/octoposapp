import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import BaseService from './base-service';
import { Observable } from 'rxjs';

@Injectable()
export class SharedTicketsService extends BaseService {

	constructor(public http: HttpClient) {
		super(http);
	}
	getSharedTicket(place: string) {
		try{
			return this.consumeService('/sharedticket/get',{place});
		}
		catch  (error) {
			console.log(error);
		}
	}

	createSharedTicket(sharedticket: any) {
		try{
			return this.consumeService('/sharedticket/create',sharedticket);
		}
		catch  (error) {
			console.log(error);
		}
	}

	updateSharedTicket(sharedticket: any) {
		try{
			return this.consumeService('/sharedticket/update',sharedticket);
		}
		catch  (error) {
			console.log(error);
		}
	}

	deleteSharedTicket(sharedticket: any) {
		try{
			return this.consumeService('/sharedticket/delete',sharedticket);
		}
		catch  (error) {
			console.log(error);
		}
	}

}