import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MainLayoutComponent } from "./shared/layout/app-layouts/main-layout.component";
import { AuthLayoutComponent } from "./shared/layout/app-layouts/auth-layout.component";

const routes: Routes = [
    {
    path: '',
    component: MainLayoutComponent,
    children: [
        {
            path: '', redirectTo: 'order', pathMatch: 'full'
        },
        {
            path: 'order',
            loadChildren: './features/pedidos/home.module#HomeModule'
        },
        
    ]
    },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
