import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeroesService, Heroe } from '../../../core/services/services-octopos/heroes.service';
import { Router } from '@angular/router';
import { ProductsService } from "@app/core/services/services-octopos/products.service";
import { CategoriesService } from "@app/core/services/services-octopos/categories.service";
import { SharedTicketsService } from "@app/core/services/services-octopos/sharedtickets.service";
import { ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { GlobalsService } from "@app/core/services/services-octopos/global.service";
import { UUID } from 'angular2-uuid';
import { TaxesService } from "@app/core/services/services-octopos/taxes.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: []
})
export class ProductsComponent implements OnInit, OnDestroy {
  products: {
    ID,
    REFERENCE,
    CODE,
    CODETYPE,
    NAME,
    PRICEBUY,
    PRICESELL,
    CATEGORY,
    TAXCAT,
    ATTRIBUTESET_ID,
    STOCKCOST,
    STOCKVOLUME,
    ISCOM,
    ISSCALE,
    ISCONSTANT,
    PRINTKB,
    SENDSTATUS,
    ISSERVICE,
    ATTRIBUTES,
    DISPLAY,
    ISVPRICE,
    ISVERPATRIB,
    TEXTTIP,
    WARRANTY,
    STOCKUNITS,
    PRINTTO,
    SUPPLIER,
    UOM,
    FLAG,
    DESCRIPTION,
    SHORT_DESCRIPTION,
    WEIGTH,
    WIDTH,
    LENGTH,
    HEIGHT,
    QUANTITY: number
  }[];
  subCategories: any[];
  orders: {
    m_sTicket: string,
    m_iLine: number,
    multiply: number,
    price: number,
    tax: {
      id: string,
      name: string,
      taxcategoryid: string,
      rate: number,
      cascade: boolean
    },
    attributes: {
      "product.taxcategoryid": string,
      "product.warranty": string,
      "product.verpatrib": string,
      "product.name": string,
      "product.service": string,
      "product.com": string,
      "product.constant": string,
      "product.texttip": string,
      "product.categoryid": string,
      "product.printer": string,
      "product.vprice": string
    },
    productid: string,
    updated: boolean


  }[];
  taxes: any[];
  constructor(private productsService: ProductsService,
    private categoriesService: CategoriesService,
    private sharedTicketsService: SharedTicketsService,
    public actRoute: ActivatedRoute,
    private _router: Router,
    private globalService: GlobalsService,
    private taxService: TaxesService,
    private location: Location) {
  }

  ngOnInit() {
    try {
      debugger
      this.orders = this.globalService.getGlobals().sharedTicked.products;
      this.actRoute.params.subscribe(params => {
        let category = params['id']
        let servicio = this.productsService.getProductCatalog(category).subscribe(
          resp => {
            debugger
            if (resp.length > 0) {
              this.products = resp;

              this.products.forEach(product => {
                product.QUANTITY = 0;
              });
              console.log(this.products);
            }

            servicio.unsubscribe();
          },
          errResponse => {
            debugger
            servicio.unsubscribe();
            throw new Error(errResponse);
          }
        );

        let servicio2 = this.categoriesService.getSubCategories(category).subscribe(
          resp => {
            debugger
            this.subCategories = resp;
            servicio2.unsubscribe();
          },
          errResponse => {
            debugger
            servicio2.unsubscribe();
            throw new Error(errResponse);
          }
        );
      })
      let servicio3 = this.taxService.getTaxes().subscribe(
        resp => {
          debugger
          this.taxes = resp;
          servicio3.unsubscribe();
        },
        errResponse => {
          debugger
          servicio3.unsubscribe();
          throw new Error(errResponse);
        }
      );

    }
    catch (error) {
      console.log(error);
    }
  }

  ngOnDestroy() {
    try {
      this.subCategories = null;
      this.products = null;
    }
    catch (error) {
      console.log(error);
    }
  }

  addQuantity(index: number) {
    try {
      this.modifyOrder(this.products[index].ID, 1)
      this.products[index].QUANTITY = this.products[index].QUANTITY + 1;
    } catch (error) {
      throw new Error('Error at add a quantity: ' + error + ``);
    }
  }

  subtractQuantity(index: number) {
    try {
      this.modifyOrder(this.products[index].ID, -1)
      if (this.products[index].QUANTITY > 0) {
        this.products[index].QUANTITY = this.products[index].QUANTITY - 1;
      }
    } catch (error) {
      throw new Error('Error at subtract a quantity: ' + error + ``);
    }
  }

  goToProducts(category: string) {
    try {
      this.subCategories = null;
      this.products = null;
      this._router.navigate(['/order/products', category]);
    }
    catch (error) {
      console.log(error);
    }
  }

  modifyOrder(productId: string, quantity: number) {
    try {
      debugger
      let index
      // if any products exist
      if (this.orders.length > 0) {
        index = this.orders.findIndex((obj => obj.productid == productId));
      } else {
        index = -1;
      }

      if (index >= 0) {
        this.orders[index].multiply = this.orders[index].multiply + quantity;
      } else {
        let product = this.products.find(item => item.ID === productId);
        let tax = this.taxes.find(obj => obj.category === product.TAXCAT)
        let newLine = {
          m_sTicket: UUID.UUID(),
          m_iLine: this.orders.length,
          multiply: quantity,
          price: product.PRICESELL,
          tax: {
            id: tax.id,
            name: tax.name,
            taxcategoryid: tax.taxcategoryid,
            rate: tax.rate,
            cascade: false//tax.ratecascade
          },
          attributes: {
            "product.taxcategoryid": product.TAXCAT,
            "product.warranty": "false",
            "product.verpatrib": "false",
            "product.name": product.NAME,
            "product.service": "false",//product.ISSERVICE,
            "product.com": "false",//product.ISCOM,
            "product.constant": "false",//product.ISCONSTANT,
            "product.texttip": "",
            "product.categoryid": product.CATEGORY,
            "product.printer": product.PRINTTO,
            "product.vprice": "false"
          },
          productid: product.ID,
          updated: false
        }
        this.orders.push(newLine);
      }
    
      // if no products exist
     /* }else{
        debugger
        // de la lista de productos, busque el producto seleccionado
        let product2 = this.products.filter(item => item.ID === product)[0];
        // de las lista de impuestos busque el impuesto del producto
          let tax = this.taxes.find(obj => obj.category === product2.TAXCAT)
          // genera la nueva linea
          let newLine = {
            m_sTicket: UUID.UUID(),
            m_iLine: this.orders.length,
            multiply: quantity,
            price: product2.PRICESELL,
            tax: {
              id: tax.id,
              name: tax.name,
              taxcategoryid: tax.category,
              rate: tax.rate,
              cascade: false//tax.ratecascade
            },
            attributes: {
              "product.taxcategoryid": product2.TAXCAT,
              "product.warranty": "false",
              "product.verpatrib": "false",
              "product.name": product2.NAME,
              "product.service": "false",//product.ISSERVICE,
              "product.com": "false",//product.ISCOM,
              "product.constant": "false",//product.ISCONSTANT,
              "product.texttip": "",
              "product.categoryid": product2.CATEGORY,
              "product.printer": product2.PRINTTO,
              "product.vprice": "false"
            },
            productid: product2.ID,
            updated: false
          }
          debugger
          this.orders.push(newLine);
      }*/

    }
  catch(error) {
    console.log(error);
  }
}
/**
 * regresa a la pagina anterior
 * @author Carlos Urrego
 */
regresar() {
  debugger
  if (this.orders.length > 0) {
    debugger
    this.globalService.globals.sharedTicked.products = this.orders;
  }
  this.location.back();
}

}
