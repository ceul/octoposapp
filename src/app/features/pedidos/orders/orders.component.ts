import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../../core/services/services-octopos/heroes.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SharedTicketsService } from "@app/core/services/services-octopos/sharedtickets.service";
import { GlobalsService } from "@app/core/services/services-octopos/global.service";
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styles: []
})
export class OrdersComponent implements OnInit {
  total: number;
  constructor(
    private sharedTicketsService: SharedTicketsService,
    private location: Location,
    public actRoute: ActivatedRoute,
    private globalService: GlobalsService,
    private _router: Router) {
  }

  ngOnInit() {
    try {
      this.total = 0;
      this.actRoute.params.subscribe(params => {
        let place = params['id']

        if (this.globalService.globals.sharedTicked == null) {
          let servicio2 = this.sharedTicketsService.getSharedTicket(place).subscribe(
            resp => {

              if (resp.length > 0) {

                this.globalService.globals.sharedTicked = resp[0];
                this.globalService.globals.sharedTicked.products = JSON.parse(this.globalService.globals.sharedTicked.products);
                this.globalService.globals.isNew = false;
                this.globalService.globals.sharedTicked.products.forEach(product => {
                  debugger
                  this.total = this.total + (product.multiply * product.price);
                });
              } else {

                let globals = this.globalService.getGlobals();
                let now = new Date();
                let hh = now.getHours();
                let min = now.getMinutes();
                let time = hh + ":" + min;

                let sharedticket = {
                  id: place,
                  name: globals.user + ' - (' + time + ' 0)',
                  appuser: globals.id,
                  pickupid: 0,
                  locked: "",
                  products: []
                }
                this.globalService.globals.sharedTicked = sharedticket;
                this.globalService.globals.isNew = true;
              }
              servicio2.unsubscribe();
            },
            errResponse => {

              servicio2.unsubscribe();
              throw new Error(errResponse);
            }
          );
        } else {
          this.globalService.globals.sharedTicked.products.forEach(product => {
            debugger
            this.total = this.total + (product.multiply * product.price);
          });
        }
      });

    }
    catch (error) {
      console.log(error);
    }
  }

  goToCategories() {
    try {
      this._router.navigate(['/order/categories']);
    }
    catch (error) {
      console.log(error);
    }
  }

  addQuantity(index: number) {
    try {
      this.globalService.globals.sharedTicked.products[index].multiply = this.globalService.globals.sharedTicked.products[index].multiply + 1;
      this.total = this.total + this.globalService.globals.sharedTicked.products[index].price;
    } catch (error) {
      throw new Error('Error at add a quantity: ' + error + ``);
    }
  }

  subtractQuantity(index: number) {
    try {
      this.globalService.globals.sharedTicked.products[index].multiply = this.globalService.globals.sharedTicked.products[index].multiply - 1;
      this.total = this.total - this.globalService.globals.sharedTicked.products[index].price;
      if (this.globalService.globals.sharedTicked.products[index].multiply <= 0) {
        this.globalService.globals.sharedTicked.products.splice(index, 1);
      }
    } catch (error) {
      throw new Error('Error at subtract a quantity: ' + error + ``);
    }
  }

  /**
	 * regresa a la pagina anterior
	 * @author Carlos Urrego
	 */
  regresar() {
    try {

      let global = this.globalService.getGlobals();
      let now = new Date();
      let hh = now.getHours();
      let min = now.getMinutes();
      let time = hh + ":" + min;
      global.sharedTicked.name = global.user + ' - (' + time + ' 0)';
      if (global.isNew) {
        let servicio = this.sharedTicketsService.createSharedTicket(global.sharedTicked).subscribe(
          resp => {
            this.globalService.globals.sharedTicked = null;
            this.globalService.globals.isNew = true;
            this.location.back();
            servicio.unsubscribe();
          },
          errResponse => {
            servicio.unsubscribe();
            throw new Error(errResponse);
          }
        );
      } else {
        let servicio1 = this.sharedTicketsService.updateSharedTicket(global.sharedTicked).subscribe(
          resp => {
            this.globalService.globals.sharedTicked = null;
            this.globalService.globals.isNew = true;
            this.location.back();
            servicio1.unsubscribe();
          },
          errResponse => {
            servicio1.unsubscribe();
            throw new Error(errResponse);
          }
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
}
