import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../../core/services/services-octopos/heroes.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoriesService } from "@app/core/services/services-octopos/categories.service";
import { Location } from '@angular/common';
import { SharedTicketsService } from "@app/core/services/services-octopos/sharedtickets.service";
import { GlobalsService } from "@app/core/services/services-octopos/global.service";
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styles: []
})
export class CategoriesComponent implements OnInit {
  categories: any[];
  
  constructor(private categoriesService: CategoriesService,
    private sharedTicketsService: SharedTicketsService,
    private location: Location,
    public actRoute: ActivatedRoute,
    private globalService: GlobalsService,
    private _router: Router) {
  }

  ngOnInit() {
    try {
      
      let servicio = this.categoriesService.getRootCategories().subscribe(
        resp => {
          debugger
          this.categories = resp;
          console.log(this.categories);
          servicio.unsubscribe();
        },
        errResponse => {
          debugger
          servicio.unsubscribe();
          throw new Error(errResponse);
        }
      );
      /*this.actRoute.params.subscribe(params => {
        let place = params['id']
        debugger
        if (this.globalService.globals.sharedTicked == null) {
          let servicio2 = this.sharedTicketsService.getSharedTicket(place).subscribe(
            resp => {
              debugger
              if (resp.length > 0) {
                debugger
                this.globalService.globals.sharedTicked = resp[0];
                this.globalService.globals.sharedTicked.products = JSON.parse(this.globalService.globals.sharedTicked.products); 
                this.globalService.globals.isNew = false;
              } else {
                debugger
                let globals = this.globalService.getGlobals();
                let now = new Date();
                let hh = now.getHours();
                let min = now.getMinutes();
                let time = hh + ":" + min;
                let sharedticket = {
                  id: place,
                  name: globals.user + ' - (' + time + ' 0)',
                  appuser: globals.id,
                  pickupid: 0,
                  locked: "",
                  products: []
                }
                this.globalService.globals.sharedTicked = sharedticket;
                this.globalService.globals.isNew = true;
              }
              servicio2.unsubscribe();
            },
            errResponse => {
              debugger
              servicio2.unsubscribe();
              throw new Error(errResponse);
            }
          );
        }
      });*/

    }
    catch (error) {
      console.log(error);
    }
  }

  goToProducts(category: string) {
    try {
      this._router.navigate(['/order/products', category]);
    }
    catch (error) {
      console.log(error);
    }
  }

  /**
	 * regresa a la pagina anterior
	 * @author Carlos Urrego
	 */
  regresar() {
    try {
      debugger
      this.location.back();
      /*let global = this.globalService.getGlobals();
      let now = new Date();
      let hh = now.getHours();
      let min = now.getMinutes();
      
      let time = hh + ":" + min;


      global.sharedTicked.name = global.user + ' - (' + time + ' 0)';
      if (global.isNew) {
        let servicio = this.sharedTicketsService.createSharedTicket(global.sharedTicked).subscribe(
          resp => {
            this.globalService.globals.sharedTicked = null;
            this.globalService.globals.isNew = true;
            this.location.back();
            servicio.unsubscribe();
          },
          errResponse => {
            servicio.unsubscribe();
            throw new Error(errResponse);
          }
        );
      } else {
        let servicio1 = this.sharedTicketsService.updateSharedTicket(global.sharedTicked).subscribe(
          resp => {
            this.globalService.globals.sharedTicked = null;
            this.globalService.globals.isNew = true;
            this.location.back();
            servicio1.unsubscribe();
          },
          errResponse => {
            servicio1.unsubscribe();
            throw new Error(errResponse);
          }
        );
      }*/
    } catch (error) {
      console.log(error);
    }
  }
}
