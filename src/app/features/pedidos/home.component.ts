import { Component, OnInit } from '@angular/core';
import { Heroe, HeroesService } from "@app/core/services/services-octopos/heroes.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(/*private heroeService:HeroesService, */private _router:Router) { 

  }

  ngOnInit(){
    // this.heroes = this.heroeService.getHeroes();
    //console.log(this.heroes);
  }

  verHeroe( idx:number ){
    //console.log(idx);
    this._router.navigate( ['/heroe', idx ]);
  }

}
