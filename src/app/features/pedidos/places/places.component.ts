import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../../core/services/services-octopos/heroes.service';
import { Router } from '@angular/router';
import { PlacesService } from "@app/core/services/services-octopos/places.service";
import { FloorsService } from "@app/core/services/services-octopos/floors.service";
import { GlobalsService } from "@app/core/services/services-octopos/global.service";

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styles: []
})
export class PlacesComponent implements OnInit {
  places: any[];
  floors: any[];

  constructor(private placesService: PlacesService,
    private floorsService: FloorsService,
    private globalService: GlobalsService,
    private _router: Router) {

  }

  ngOnInit() {
    try {
      debugger
      let servicio2 = this.floorsService.getFloors().subscribe(
        resp => {
          debugger
          this.floors = resp;
          let servicio = this.placesService.getPlaces(this.floors[0].ID).subscribe(
            resp => {
              this.places = resp;
              servicio.unsubscribe();
            },
            errResponse => {
              servicio.unsubscribe();
              throw new Error(errResponse);
            }
          );
          servicio2.unsubscribe();
        },
        errResponse => {
          debugger
          servicio2.unsubscribe();
          throw new Error(errResponse);
        }
      );

    }
    catch (error) {
      console.log(error);
    }
  }

  goToCategories(id: string) {
    try {
      //this.globalService.setPlace(id);
      this._router.navigate(['/order/orders', id]);
    }
    catch (error) {
      console.log(error);
    }
  }
}
