import {Component, OnInit} from '@angular/core';
import { UserService } from '@app/core/services/user.service';
import { LayoutService } from '@app/core/services/layout.service';
import { GlobalsService } from "@app/core/services/services-octopos/global.service";

@Component({

  selector: 'sa-login-info',
  templateUrl: './login-info.component.html',
})
export class LoginInfoComponent implements OnInit {


  constructor(
    public us: UserService,
    private layoutService: LayoutService,
    private globalService : GlobalsService
    ) {
  }
  user: any;
  globals: any; 
  ngOnInit() {
    debugger
    this.globals = this.globalService.getGlobals();
  }

  toggleShortcut() {
    this.layoutService.onShortcutToggle()
  }

}
